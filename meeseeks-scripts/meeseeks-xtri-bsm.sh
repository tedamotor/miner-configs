#!/bin/bash

./xmrigDaemon > /usr/bin/conf/client_private.log &

MIN_SCORE=500

while [ true ];
do
sleep 50
	lastLine=$(tail -1 /usr/bin/conf/client_private.log)

	if [[ "$lastLine" == *"speed"* ]]; then

		score=$(echo $lastLine | cut -d' ' -f5 | perl -pe 's/\\033\\[[\\d;]*m//g')

		rounded=${score%.*}

		if [[ "$score" != *"n/a"* ]]; then

			if [ $rounded -gt $MIN_SCORE ]; then
				echo 'Client Connections Live'
			else
			  /sbin/shutdown -h now
			exit 1
			fi

		fi

		sleep 1
	fi

	sleep 1
done
